import { expect } from "chai";
import { ethers } from "hardhat";
import { Token__factory } from "../typechain-types/factories/contracts/Token__factory";
import { Token } from "../typechain-types/contracts/Token";
import { Vesting__factory } from "../typechain-types/factories/contracts/Vesting__factory";
import { Vesting } from "../typechain-types/contracts/Vesting";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { mine } from "@nomicfoundation/hardhat-network-helpers";
import { parseUnits } from "ethers/lib/utils";
import { BigNumber, ContractTransaction } from "ethers";

async function checkBatchVestingCreatedEvent(
  tx: ContractTransaction,
  vesting: Vesting,
  accounts: string[],
  amounts: BigNumber[],
  tge: number | BigNumber,
  cliff: number | BigNumber,
  vestingSeconds: number | BigNumber,
  createdAt: number | BigNumber,
  direction: number | BigNumber) {
  const topic = ethers.utils.id("BatchVestingCreated(address[],uint256[],uint128,uint128,uint128,uint16,uint8)");
  const event = (await tx.wait()).events!.find(event => event.topics[0] === topic)!;
  const decodedEvent = vesting.interface.decodeEventLog(topic, event.data, event.topics);
  
  expect(decodedEvent.accounts.length).to.be.equal(accounts.length);
  for (let i = 0; i < accounts.length; i++) {
    expect(decodedEvent.accounts[i]).to.be.equal(accounts[i]);
  }
  expect(decodedEvent.amounts.length).to.be.equal(amounts.length);
  for (let i = 0; i < amounts.length; i++) {
    expect(decodedEvent.amounts[i]).to.be.equal(amounts[i]);
  }
  expect(decodedEvent.tge).to.be.equal(tge);
  expect(decodedEvent.cliff).to.be.equal(cliff);
  expect(decodedEvent.vesting).to.be.equal(vestingSeconds);
  expect(decodedEvent.createdAt).to.be.equal(createdAt);
  expect(decodedEvent.direction).to.be.equal(direction);
}

async function incrementNextBlockTimestamp(amount: number): Promise<void> {
  return ethers.provider.send("evm_increaseTime", [amount]);
}

async function getBlockTimestamp(tx: any): Promise<number> {
  const minedTx = await tx.wait();
  const txBlock = await ethers.provider.getBlock(minedTx.blockNumber);
  return txBlock.timestamp;
}

describe('Vesting contract', () => {
  let token: Token;
  let vesting: Vesting;
  let owner: SignerWithAddress;
  let addr1: SignerWithAddress;
  let addr2: SignerWithAddress;
  let addrs: SignerWithAddress[];
  const name = "Orange Token";
  const symbol = "OT";
  const zeroAddress = '0x0000000000000000000000000000000000000000';
  const totalSupply = parseUnits("809710000", 18);

  enum Direction {
    PRIVATE_ROUND_A,
    PRIVATE_ROUND_B,
    COMMUNITY_ROUND,
    KOL,
    MARKETING,
    TEAM,
    ECOSYSTEMRD,
    ADVISORS
  }

  beforeEach(async () => {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const Token = (await ethers.getContractFactory('Token')) as Token__factory;
    token = await Token.deploy(name, symbol, owner.address);
    await token.deployed();

    const Vesting = (await ethers.getContractFactory('Vesting')) as Vesting__factory;
    vesting = await Vesting.deploy(token.address, owner.address);
    await vesting.deployed();
  });

  describe('initial values', async () => {
    it('should set roles', async () => {
      /* ASSERT */
      expect(await vesting.hasRole(await vesting.DEFAULT_ADMIN_ROLE(), owner.address)).to.true;
      expect(await vesting.hasRole(await vesting.STARTER_ROLE(), token.address)).to.true;
    })
  });

  describe('starting vesting', async () => {
    it('rejects if vesting not started', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* EXECUTE */
      await token.connect(owner).transfer(vesting.address, parseUnits("100000000", await token.decimals()));

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "NotStarted()"
      );
    });

    it('rejects if not starter role', async () => {
      /* ASSERT */
      await expect(vesting.connect(addr1).setStartAt()).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.STARTER_ROLE()}`
      );
    });
  });

  describe('sets community round vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets community round vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;
      const direction = Direction.COMMUNITY_ROUND;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets community round vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("200", 18)];
      const amountsB = [parseUnits("100", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 10000;
      const direction = Direction.COMMUNITY_ROUND;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setCommunityRoundVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setCommunityRoundVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = '-1';
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(addr1).setCommunityRoundVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets private round A vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets private round A vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;
      const direction = Direction.PRIVATE_ROUND_A;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets private round A vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 10000;
      const direction = Direction.PRIVATE_ROUND_A;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("200", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds);
      const tx = await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds);
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 0;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;

      /* ASSERT */
      await expect(vesting.connect(addr1).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets private round B vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets private round B vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;
      const direction = Direction.PRIVATE_ROUND_B;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets private round B vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("200", 18)];
      const amountsB = [parseUnits("100", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 10000;
      const direction = Direction.PRIVATE_ROUND_B;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundBVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setPrivateRoundBVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 0;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(addr1).setPrivateRoundBVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets KOL vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets KOL vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;
      const direction = Direction.KOL;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets KOL vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("90", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 10000;
      const direction = Direction.KOL;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("90", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setKOLVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setKOLVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)

      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 0;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(owner).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 15552000;
      const vestingSeconds = 62208000;
      const tge = 1000;

      /* ASSERT */
      await expect(vesting.connect(addr1).setKOLVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets marketing vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets marketing vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;
      const direction = Direction.MARKETING;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));
      const startAt = await vesting.startAt();

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets marketing vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("200", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 10000;
      const direction = Direction.MARKETING;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("200", 18);
      const startAt = await vesting.startAt();

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)

      const tx = await vesting.connect(owner).setMarketingVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 62208000;
      const tge = 166;

      /* ASSERT */
      await expect(vesting.connect(addr1).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets team vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets team vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;
      const direction = Direction.TEAM;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets team vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("300", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 10000;
      const direction = Direction.TEAM;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("300", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setTeamVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setTeamVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(addr1).setTeamVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets ecosystem R&D vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets ecosystem R&D vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;
      const direction = Direction.ECOSYSTEMRD;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets ecosystem R&D vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("300", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 10000;
      const direction = Direction.ECOSYSTEMRD;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("300", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setEcosystemRDVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setEcosystemRDVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(addr1).setEcosystemRDVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('sets advisors vest for', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('sets advisors vest for successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;
      const direction = Direction.ADVISORS;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      const tx = await vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amounts, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('sets advisors vest for again successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("200", 18)];
      const amountsB = [parseUnits("100", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 10000;
      const direction = Direction.ADVISORS;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18);

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountBefore));

      /* EXECUTE */
      await vesting.connect(owner).setAdvisorsVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds)
      const tx = await vesting.connect(owner).setAdvisorsVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds)
      const createdAt = await getBlockTimestamp(tx);

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amountsB[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }

      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
      expect(await vesting.getWithdrawableAmount()).to.equal((await token.balanceOf(vesting.address)).sub(totalAmountAfter));
      await checkBatchVestingCreatedEvent(tx, vesting, accounts, amountsB, tge, cliffSeconds, vestingSeconds, createdAt, direction);
    });

    it('rejects if accounts and amounts lengths is zero', async () => {
      /* SETUP */
      const cliffSeconds = 0;
      const vestingSeconds = 15552000;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor([], [], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsIsZero()"
      );
    });

    it('rejects if accounts and amounts lengths not match', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DataLengthsNotMatch()"
      );
    });

    it('rejects if not sufficient tokens', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("900000000", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "InsufficientTokens()"
      );
    });

    it('rejects if incorrect amount', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("0", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectAmount()"
      );
    });

    it('rejects if vesting period is zero', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "DurationIsZero()"
      );
    });

    it('rejects if vesting period is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = -1;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if cliff is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = -1;
      const vestingSeconds = 10;
      const tge = 2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });
    
    it('rejects if tge is negative', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 0;
      const tge = -2000;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.reverted;
    });

    it('rejects if tge is higher than 100%', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 10;
      const tge = 10001;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "IncorrectTGE()"
      );
    });

    it('rejects if new totalAmount less than claimed', async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, [claimedBefore.sub(parseUnits("1", 18))], tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "TotalAmountLessThanClaimed()"
      );
    });

    it('rejects if zero vester address', async () => {
      /* SETUP */
      const accounts = [zeroAddress, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(owner).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        "ZeroAddress()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 49248000;
      const tge = 500;

      /* ASSERT */
      await expect(vesting.connect(addr1).setAdvisorsVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      ).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });
  });

  describe('withdraws', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('withdraws successfully', async () => {
      /* SETUP */
      const amount = parseUnits("100", 18);
      const withdrawableAmountBefore = await vesting.getWithdrawableAmount();

      /* ASSERT */
      expect(withdrawableAmountBefore).to.equal((await token.balanceOf(vesting.address)));

      /* EXECUTE */
      const tx = await vesting.connect(owner).withdraw(amount);

      /* ASSERT */
      expect(await vesting.getWithdrawableAmount()).to.equal(withdrawableAmountBefore.sub(amount));
      await expect(tx).to.emit(token, "Transfer")
        .withArgs(vesting.address, owner.address, amount);
    });

    it('rejects if not enough funds', async () => {
      /* SETUP */
      const amount = (await vesting.getWithdrawableAmount()).add(parseUnits("100", 18));

      /* ASSERT */
      await expect(vesting.connect(owner).withdraw(amount)).to.be.revertedWith(
        "NotEnoughFunds()"
      );
    });

    it('rejects if not default admin role', async () => {
      /* SETUP */
      const amount = parseUnits("100", 18);

      /* ASSERT */
      await expect(vesting.connect(addr1).withdraw(amount)).to.be.revertedWith(
        `AccessControl: account ${addr1.address.toLowerCase()} is missing role ${await vesting.DEFAULT_ADMIN_ROLE()}`
      );
    });

    it("reverts on reentrancy attack", async function () {
      /* EXECUTE */
      const ReentrantToken = await ethers.getContractFactory("ReentrantToken");
      const reentrantToken = await ReentrantToken.deploy();

      const Vesting = (await ethers.getContractFactory('Vesting')) as Vesting__factory;
      const vulnerableVesting = await Vesting.deploy(reentrantToken.address, owner.address);
      await vesting.deployed();

      const tx = vulnerableVesting.withdraw(0);

      /* ASSERT */
      expect(tx).to.be.revertedWith("ReentrancyGuard: reentrant call");
    })
  });

  describe('gets withdrawable amount', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('gets withdrawable amount successfully', async () => {
      /* SETUP */
      const amount = parseUnits("100", 18);
      const withdrawableAmountBefore = await vesting.getWithdrawableAmount();

      /* ASSERT */
      expect(withdrawableAmountBefore).to.equal((await token.balanceOf(vesting.address)));

      /* EXECUTE */
      await vesting.connect(owner).withdraw(amount);

      /* ASSERT */
      expect(await vesting.getWithdrawableAmount()).to.equal(withdrawableAmountBefore.sub(amount));
    });
  });

  describe('gets vesting schedules total amount', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('gets vesting schedules total amount successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;
      const totalAmountBefore = 0;
      const totalAmountAfter = parseUnits("100", 18).add(parseUnits("200", 18));

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountBefore);

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      /* ASSERT */
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(totalAmountAfter);
    });
  });

  describe('gets vesting schedule', async () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it('gets vesting schedule successfully', async () => {
      /* SETUP */
      const accounts = [addr1.address, addr2.address];
      const amounts = [parseUnits("100", 18), parseUnits("200", 18)];
      const startAt = await vesting.startAt();
      const cliffSeconds = 31104000;
      const vestingSeconds = 82944000;
      const tge = 0;
      const direction = Direction.PRIVATE_ROUND_A;

      /* EXECUTE */
      await vesting.connect(owner).setPrivateRoundAVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      /* ASSERT */
      for (let i = 0; i < accounts.length; i++) {
        const vestedScheduleAfter = await vesting.vestingSchedules(accounts[i], direction);

        expect(vestedScheduleAfter.cliffInSeconds).to.equal(cliffSeconds);
        expect(vestedScheduleAfter.startAt).to.equal(startAt);
        expect(vestedScheduleAfter.vestingInSeconds).to.equal(vestingSeconds);
        expect(vestedScheduleAfter.totalAmount).to.equal(amounts[i]);
        expect(vestedScheduleAfter.claimed).to.equal(0);
        expect(vestedScheduleAfter.tge).to.equal(tge);
      }
    });
  });

  describe('gets total vesting info', () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it("gets info if cliff doesn't finish", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const tge = 166;
      const cliffSeconds = 500;
      const vestingSeconds = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds);

      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.equal(amounts[0].mul(tge).div(10000));
      expect(claimedAmount).to.be.equal(0);
      expect(lockedAmount).to.be.equal(amounts[0].sub(amounts[0].mul(tge).div(10000)));
    });

    it("gets info if TGE - 2%, cliffSeconds - 0, claimed - 0", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 25920000;
      const tge = 200;
      const timestampAfter = 7948800;
      const startAt = await vesting.startAt();
      
      /* EXECUTE */
      const tx = await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds);
      const createdAt = await getBlockTimestamp(tx);

      await incrementNextBlockTimestamp(timestampAfter);
      await mine();

      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const tgeAmount = amounts[0].mul(tge).div(10000);
      const vestedAmount = ((amounts[0].sub(tgeAmount)).mul(createdAt - startAt.toNumber() + timestampAfter)).div(vestingSeconds).add(tgeAmount);

      /* ASSERT */
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.not.equal(0);
      expect(unlockedAmount).to.be.equal(vestedAmount);
      expect(claimedAmount).to.be.equal(0);
      expect(lockedAmount).to.be.equal(amounts[0].sub(unlockedAmount));
    });

    it("gets info if vesting completed fully", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 166;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      await incrementNextBlockTimestamp(2592001);
      await mine();

      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.not.equal(0);
      expect(unlockedAmount).to.be.equal(amounts[0]);
      expect(claimedAmount).to.be.equal(0);
      expect(lockedAmount).to.be.equal(0);
    });

    it("gets info if claimed = totalAmount", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("10000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 1000;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds);
      await vesting.connect(addr1).claim();

      const [, , claimedBefore,] = await vesting.getTotalVestingInfo(accounts[0]);

      await vesting.connect(owner).setMarketingVestFor(accounts, [claimedBefore], tge, cliffSeconds, vestingSeconds);

      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      /* ASSERT */
      expect(totalAmount).to.be.equal(claimedBefore);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(claimedBefore);
      expect(lockedAmount).to.be.equal(0);
    });
  });

  describe('claims', () => {
    beforeEach(async () => {
      await token.connect(owner).executeTGE(vesting.address, totalSupply);
    });

    it("claims with part rewards successfully", async () => {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 25920000;
      const tge = 166;
      const direction = Direction.MARKETING;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds);
      const startAt = await vesting.startAt();

      await incrementNextBlockTimestamp(7776000);
      await mine();

      const addr1BalanceBefore = await token.balanceOf(accounts[0]);
      const vestingSchedulesTotalAmountBefore = await vesting.vestingSchedulesTotalAmount();
      const vestingSchedulesBefore = await vesting.vestingSchedules(accounts[0], direction);

      const tx = await vesting.connect(addr1).claim();
      const timestampAfter = await getBlockTimestamp(tx);

      const vestingSchedulesAfter = await vesting.vestingSchedules(accounts[0], direction);
      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const tgeAmount = amounts[0].mul(tge).div(10000);
      const vestedAmount = ((amounts[0].sub(tgeAmount)).mul(timestampAfter - startAt.toNumber())).div(vestingSeconds).add(tgeAmount);
      const addr1BalanceAfter = await token.balanceOf(accounts[0]);

      /* ASSERT */
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(vestedAmount);
      expect(lockedAmount).to.be.equal(amounts[0].sub(vestedAmount));
      expect(addr1BalanceAfter).to.be.equal(addr1BalanceBefore.add(vestedAmount));
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(vestingSchedulesTotalAmountBefore.sub(vestedAmount));
      expect(vestingSchedulesAfter.claimed).to.be.equal(vestingSchedulesBefore.claimed.add(vestedAmount));

      await expect(tx).to.emit(vesting, 'Claimed')
        .withArgs(addr1.address, vestedAmount, timestampAfter, direction);
    });

    it("claims with all rewards successfully", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("200", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 600;
      const tge = 3000;
      const direction = Direction.MARKETING;

      /* EXECUTE */
      let tx = await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      await incrementNextBlockTimestamp(2592000);
      await mine();

      const addr1BalanceBefore = await token.balanceOf(accounts[0]);

      const vestingSchedulesTotalAmountBefore = await vesting.vestingSchedulesTotalAmount();
      const vestingSchedulesBefore = await vesting.vestingSchedules(accounts[0], direction);

      tx = await vesting.connect(addr1).claim();
      const timestampAfter = await getBlockTimestamp(tx);

      const vestingSchedulesAfter = await vesting.vestingSchedules(accounts[0], direction);
      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const vestedAmount = amounts[0];
      const addr1BalanceAfter = await token.balanceOf(accounts[0]);

      /* ASSERT */
      expect(addr1BalanceAfter).to.be.equal(addr1BalanceBefore.add(vestedAmount));
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(vestingSchedulesTotalAmountBefore.sub(vestedAmount));
      expect(vestingSchedulesAfter.claimed).to.be.equal(vestingSchedulesBefore.claimed.add(vestedAmount));
      expect(totalAmount).to.be.equal(vestedAmount);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(vestedAmount);
      expect(lockedAmount).to.be.equal(0);
      await expect(tx).to.emit(vesting, 'Claimed')
        .withArgs(accounts[0], vestedAmount, timestampAfter, direction);
    });

    it("claims from 2 directions successfully", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("100", 18)];
      const amountsB = [parseUnits("200", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 166;
      const directionA = Direction.MARKETING;
      const directionB = Direction.COMMUNITY_ROUND;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds);
      await vesting.connect(owner).setCommunityRoundVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds);

      await incrementNextBlockTimestamp(2592000);
      await mine();

      const addr1BalanceBefore = await token.balanceOf(accounts[0]);

      const vestingSchedulesTotalAmountBefore = await vesting.vestingSchedulesTotalAmount();

      const tx = await vesting.connect(addr1).claim();
      const timestampAfter = await getBlockTimestamp(tx);

      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const vestedAmount = amountsA[0].add(amountsB[0]);
      const addr1BalanceAfter = await token.balanceOf(accounts[0]);

      /* ASSERT */
      expect(addr1BalanceAfter).to.be.equal(addr1BalanceBefore.add(vestedAmount));
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(vestingSchedulesTotalAmountBefore.sub(vestedAmount));
      expect(totalAmount).to.be.equal(vestedAmount);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(vestedAmount);
      expect(lockedAmount).to.be.equal(0);
      await expect(tx).to.emit(vesting, 'Claimed')
        .withArgs(accounts[0], amountsA[0], timestampAfter, directionA)
        .to.emit(vesting, 'Claimed')
        .withArgs(accounts[0], amountsB[0], timestampAfter, directionB);
    });

    it("claims tge - 1% when cliff - 0, tge - 1%, cliff period doesn't finished", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 100;
      const vestingSeconds = 10000;
      const tge = 100;
      const direction = Direction.MARKETING;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      const addr1BalanceBefore = await token.balanceOf(accounts[0]);
      const vestingSchedulesTotalAmountBefore = await vesting.vestingSchedulesTotalAmount();
      const vestingSchedulesBefore = await vesting.vestingSchedules(accounts[0], direction);

      const tx = await vesting.connect(addr1).claim();
      const timestampAfter = await getBlockTimestamp(tx);

      const vestingSchedulesAfter = await vesting.vestingSchedules(accounts[0], direction);
      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const vestedAmount = amounts[0].mul(tge).div(10000);
      const addr1BalanceAfter = await token.balanceOf(accounts[0]);

      /* ASSERT */
      expect(addr1BalanceAfter).to.be.equal(addr1BalanceBefore.add(vestedAmount));
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(vestingSchedulesTotalAmountBefore.sub(vestedAmount));
      expect(vestingSchedulesAfter.claimed).to.be.equal(vestingSchedulesBefore.claimed.add(vestedAmount));
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(vestedAmount);
      expect(lockedAmount).to.be.equal(amounts[0].sub(vestedAmount));
      await expect(tx).to.emit(vesting, 'Claimed')
        .withArgs(accounts[0], vestedAmount, timestampAfter, direction);
    });

    it("claims tge - 1% when cliff - 0, tge - 1%, claimed - 1%, vesting period doesn't finished", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 100;
      const vestingSeconds = 10000;
      const tge = 100;
      const direction = Direction.MARKETING;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)
      const startAt = await vesting.startAt();

      await vesting.connect(addr1).claim();

      await incrementNextBlockTimestamp(cliffSeconds);
      await mine();

      const addr1BalanceBefore = await token.balanceOf(accounts[0]);
      const vestingSchedulesTotalAmountBefore = await vesting.vestingSchedulesTotalAmount();
      const vestingSchedulesBefore = await vesting.vestingSchedules(accounts[0], direction);

      const tx = await vesting.connect(addr1).claim();

      const timestampAfter = await getBlockTimestamp(tx);
      const vestingSchedulesAfter = await vesting.vestingSchedules(accounts[0], direction);
      const [totalAmount, unlockedAmount, claimedAmount, lockedAmount] = await vesting.getTotalVestingInfo(accounts[0]);

      const claimed = amounts[0].mul(tge).div(10000);
      const vestedAmount = (((amounts[0].sub(claimed)).div(vestingSeconds)).mul(timestampAfter - startAt.toNumber() - cliffSeconds));
      const addr1BalanceAfter = await token.balanceOf(accounts[0]);

      /* ASSERT */
      expect(addr1BalanceAfter).to.be.equal(addr1BalanceBefore.add(vestedAmount));
      expect(await vesting.vestingSchedulesTotalAmount()).to.equal(vestingSchedulesTotalAmountBefore.sub(vestedAmount));
      expect(vestingSchedulesAfter.claimed).to.be.equal(vestingSchedulesBefore.claimed.add(vestedAmount));
      expect(totalAmount).to.be.equal(amounts[0]);
      expect(unlockedAmount).to.be.equal(0);
      expect(claimedAmount).to.be.equal(vestedAmount.add(claimed));
      expect(lockedAmount).to.be.equal(amounts[0].sub(vestedAmount).sub(claimed));
      await expect(tx).to.emit(vesting, 'Claimed')
        .withArgs(accounts[0], vestedAmount, timestampAfter, direction);
    });

    it("rejects if claming when amount - 0, cliff - 200, tge - 0%, vesting doesn't finish", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amounts = [parseUnits("100", 18)];
      const cliffSeconds = 200;
      const vestingSeconds = 1000;
      const tge = 0;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amounts, tge, cliffSeconds, vestingSeconds)

      /* ASSERT */
      await expect(vesting.connect(addr1).claim()).to.be.revertedWith(
        "ClaimAmountIsZero()"
      );
    });

    it("rejects if claming when amount - 0, address is not in any of directions", async function () {

      /* ASSERT */
      await expect(vesting.connect(addr1).claim()).to.be.revertedWith(
        "ClaimAmountIsZero()"
      );
    });

    it("rejects if claming when amount - 0, cliff - 0, tge - 1%, claimed = totalAmount, vesting doesn't finish", async function () {
      /* SETUP */
      const accounts = [addr1.address];
      const amountsA = [parseUnits("10000", 18)];
      const amountsB = [parseUnits("1000", 18)];
      const cliffSeconds = 0;
      const vestingSeconds = 1000;
      const tge = 100;

      /* EXECUTE */
      await vesting.connect(owner).setMarketingVestFor(accounts, amountsA, tge, cliffSeconds, vestingSeconds);
      await vesting.connect(addr1).claim();
      await vesting.connect(owner).setMarketingVestFor(accounts, amountsB, tge, cliffSeconds, vestingSeconds);

      /* ASSERT */
      await expect(vesting.connect(addr1).claim()).to.be.revertedWith(
        "ClaimAmountIsZero()"
      );
    });
  });
});
