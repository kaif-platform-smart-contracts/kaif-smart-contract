import * as fs from "fs";
import * as dotenv from "dotenv";
import { ethers } from "hardhat";

dotenv.config();

const BATCH_ACCOUNTS_DATA_PATH = "scripts/data-validation/accounts_data.csv";
const VALIDATED_ACCOUNTS_PATH = "scripts/data-validation/validated_accounts.csv";

const csv = fs.readFileSync(BATCH_ACCOUNTS_DATA_PATH, "utf-8");
const rows = csv.split("\n").map((row) => row.split(","));

async function main() {
  const stream = fs.createWriteStream(VALIDATED_ACCOUNTS_PATH, { flags: 'w' });

  stream.write(`${'account'}\n`);

  for (const row of rows.slice(1)) {
    const [account] = row;

    try {
      ethers.utils.getAddress(account);
      stream.write(`${account}\n`);
    } catch (error) {
      //console.error(`Invalid address: ${account}`);
    }
  }
  stream.close();
}

main().then(() => console.log('Validation completed successfully.'))
.catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
