import hre, { ethers } from "hardhat";
import { Vesting__factory } from "../typechain-types/factories/contracts/Vesting__factory";
import { Vesting } from "../typechain-types/contracts/Vesting";

async function main() {
  const delay = (ms: any) => new Promise((res) => setTimeout(res, ms));

  let vesting: Vesting;
  const tokenAddress = "0x2074993C80Eff3DF63e3805da2D86E572de9d877";
  const owner = '0x1BBc359d826D2B4707CcfE7c51BEb9428644FDAf';

  const Vesting = (await ethers.getContractFactory('Vesting')) as Vesting__factory;
  vesting = await Vesting.deploy(tokenAddress, owner);
  await vesting.deployed();

  console.log("Vesting deployed to:", vesting.address);

  await delay(35000);

  await hre.run("verify:verify", {
    address: vesting.address,
    constructorArguments: [tokenAddress, owner],
  });
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
