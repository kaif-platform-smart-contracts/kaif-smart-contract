import hre, { ethers } from "hardhat";
import { Token__factory } from "../typechain-types/factories/contracts/Token__factory";
import { Token } from "../typechain-types/contracts/Token";

async function main() {
  const delay = (ms: any) => new Promise((res) => setTimeout(res, ms));

  let token: Token;
  const name = "Good Morning";
  const symbol = "GM";
  const to = '0x1BBc359d826D2B4707CcfE7c51BEb9428644FDAf';

  const Token = (await ethers.getContractFactory('Token')) as Token__factory;
  token = await Token.deploy(name, symbol, to);
  await token.deployed();

  console.log("Token deployed to:", token.address);

  await delay(35000);

  await hre.run("verify:verify", {
    address: token.address,
    constructorArguments: [name, symbol, to],
  });
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
