import * as fs from "fs";
import * as dotenv from "dotenv";
import { ethers } from "ethers";

dotenv.config();

const PROVIDER = process.env.BSC_RPC_URL_FOR_REPORT as string;
const CLAIMED_EVENT_REPORT = process.env.CLAIMED_EVENT_REPORT as string;
const CONTRACT_ADDRESS = process.env.VESTING_CONTRACT_ADDRESS as string;
const HASH_TGE_EXECUTION = process.env.HASH_TGE_EXECUTION as string;
const BATCH_SIZE_FOR_GETTING_LOGS = parseInt(process.env.BATCH_SIZE_FOR_GETTING_LOGS as string);

const vestingAbi = [
    'event Claimed(address account, uint256 amount, uint128 createdAt, uint8 direction)',
];

const provider = new ethers.providers.JsonRpcProvider(PROVIDER);
const vestingContract = new ethers.Contract(CONTRACT_ADDRESS, vestingAbi, provider);

const stream = fs.createWriteStream(CLAIMED_EVENT_REPORT, { flags: 'w' });

async function getEvents(fromBlock: number, toBlock: number) {
    const filter = vestingContract.filters.Claimed(null, null, null);
    return vestingContract.queryFilter(filter, fromBlock, toBlock);
}

async function saveDataToFile(events: ethers.Event[]) {
    events.forEach(async (event) => {
        const decodedLog = vestingContract.interface.decodeEventLog('Claimed', event.data, event.topics);

        const date = decodedLog.createdAt;
        const account = decodedLog.account;
        const amount = decodedLog.amount;
        const pool = decodedLog.direction;

        const row = [account, pool, amount, date];

        stream.write(`${row.join(',')}\n`);
    });
}

async function main() {
    let events = [];
    const batchSize = BATCH_SIZE_FOR_GETTING_LOGS;
    const latestBlock = await provider.getBlockNumber();
    const tx = await provider.getTransaction(HASH_TGE_EXECUTION);

    let fromBlock = tx.blockNumber;

    if (!fromBlock) {
        throw new Error('No data');
    }

    let toBlock = fromBlock + batchSize;

    while (fromBlock <= latestBlock) {
        if (toBlock > latestBlock) {
            toBlock = latestBlock;
        }

        if (events) {
            events.push(await getEvents(fromBlock, toBlock));
        }

        fromBlock = toBlock + 1;
        toBlock = fromBlock + batchSize;
    }

    const headers = ['Wallet', 'Pool', 'QTY', 'Date'];
    stream.write(`${headers.join(',')}\n`);

    events.forEach(async (element: any) => {
        await saveDataToFile(element);
    });

    stream.close();
}

main()
    .then(() => console.log('Vesting data saved to file.'))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });