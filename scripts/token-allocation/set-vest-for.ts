import * as fs from "fs";
import * as dotenv from "dotenv";
import { ethers } from "hardhat";
import { BigNumber, Contract } from "ethers";

dotenv.config();

const PROVIDER = process.env.BSC_RPC_UR_FOR_ADDING_TO_POOL as string;
const CONTRACT_ADDRESS = process.env.VESTING_CONTRACT_ADDRESS as string;
const PRIVATE_KEY = process.env.PRIVATE_KEY as string;
const BATCH_SIZE_FOR_ADDING_TO_POOL = parseInt(process.env.BATCH_SIZE_FOR_ADDING_TO_POOL as string);
const VESTING_ABI_PATH = "scripts/token-allocation/vesting_abi.json";
const VESTING_DATA_PATH = "scripts/token-allocation/vesting_data.csv";
const MULTIPLIER_GAS_ESTIMATE = parseInt(process.env.MULTIPLIER_GAS_ESTIMATE as string);
const blockGasLimit = 140000000;

const web3Provider = new ethers.providers.JsonRpcProvider(PROVIDER);
const signer = new ethers.Wallet(PRIVATE_KEY as string, web3Provider);
const abi = JSON.parse(fs.readFileSync(VESTING_ABI_PATH, "utf8"));

const vesting = new ethers.Contract(CONTRACT_ADDRESS as string, abi, signer);

const csv = fs.readFileSync(VESTING_DATA_PATH, "utf-8");
const rows = csv.split("\n").map((row) => row.split(","));

interface VestingParams {
  pool: number;
  tge: number;
  cliffInSeconds: number;
  vestingInSeconds: number;
  accounts: string[];
  amounts: string[];
}

let map = new Map<string, VestingParams>();

enum Pool {
  PRIVATE_ROUND_A,
  PRIVATE_ROUND_B,
  COMMUNITY_ROUND,
  KOL,
  MARKETING,
  TEAM,
  ECOSYSTEMRD,
  ADVISORS
}

async function main() {
  const start = Date.now();
  console.log(`Start time taken: ${start / 1000} seconds`);

  try {
    for (const row of rows.slice(1)) {
      const [account, amount, tge, cliffInSeconds, vestingInSeconds, poolNotParsed] = row;
      const pool = parseInt(poolNotParsed).toString();

      const id = `${pool}/${tge}/${cliffInSeconds}/${vestingInSeconds}`;

      let currentParams = map.get(id);
      if (currentParams) {
        currentParams.accounts.push(account);
        currentParams.amounts.push(amount);

        map.set(id, currentParams);
      } else {
        let params: VestingParams = {
          pool: parseInt(pool),
          tge: parseInt(tge),
          cliffInSeconds: parseInt(cliffInSeconds),
          vestingInSeconds: parseInt(vestingInSeconds),
          accounts: [account],
          amounts: [amount],
        }
        map.set(id, params);
      }
    }

    await setVestFor(vesting, map, signer);

    console.log(`All transactions completed successfully!`);
    console.log("size", map.size);
    const end = Date.now();
    console.log(`Time taken: ${(end - start) / 1000} seconds`);
  } catch (error) {
    console.log((<Error>error).message);
    const end = Date.now();
    console.log(`End time taken: ${end / 1000} seconds`);
  }
}

async function setVestFor(vesting: Contract, map: Map<string, VestingParams>, signer: any) {
  let totalEstimatedGas: BigNumber = ethers.BigNumber.from(0);
  for (const [key, params] of map) {

    const methodName = getMethodNameByPool(params.pool);
    const accounts = params.accounts;
    const amounts = params.amounts;

    for (let i = 0; i < accounts.length; i += BATCH_SIZE_FOR_ADDING_TO_POOL) {
      const accountsBatch = accounts.slice(i, i + BATCH_SIZE_FOR_ADDING_TO_POOL);
      const amountsBatch = amounts.slice(i, i + BATCH_SIZE_FOR_ADDING_TO_POOL);

      const estimatedGas = await vesting.estimateGas[methodName](
        accountsBatch,
        amountsBatch,
        params.tge,
        params.cliffInSeconds,
        params.vestingInSeconds,
      );

      const gasLimit = estimatedGas.mul(MULTIPLIER_GAS_ESTIMATE).div(100);

      if (gasLimit.gt(blockGasLimit)) {
        console.log('gasLimit', gasLimit);
        throw new Error("Transaction failed #1: insufficient blockGasLimit!");
      }

      totalEstimatedGas = totalEstimatedGas.add(estimatedGas);
    }
  }

  const gasLimit = totalEstimatedGas.mul(MULTIPLIER_GAS_ESTIMATE).div(100);

  const gasPrice = await web3Provider.getGasPrice();
  const estimatedCost = gasLimit.mul(gasPrice);
  const balance = await web3Provider.getBalance(signer.address);

  if (estimatedCost.gt(balance)) {
    console.log('Balance', balance);
    console.log('EstimatedCost', estimatedCost);
    throw new Error("Transaction failed #1: insufficient funds!");
  }

  for (const [key, params] of map) {
    const methodName = getMethodNameByPool(params.pool);
    const accounts = params.accounts;
    const amounts = params.amounts;

    for (let i = 0; i < accounts.length; i += BATCH_SIZE_FOR_ADDING_TO_POOL) {
      const accountsBatch = accounts.slice(i, i + BATCH_SIZE_FOR_ADDING_TO_POOL);
      const amountsBatch = amounts.slice(i, i + BATCH_SIZE_FOR_ADDING_TO_POOL);

      const methodParams = [
        accountsBatch,
        amountsBatch,
        params.tge,
        params.cliffInSeconds,
        params.vestingInSeconds,
      ];

      const estimatedGas = await vesting.estimateGas[methodName](...methodParams);

      const gasLimit = estimatedGas.mul(MULTIPLIER_GAS_ESTIMATE).div(100);
      const gasPrice = await web3Provider.getGasPrice();
      const estimatedCost = gasLimit.mul(gasPrice);
      const balance = await web3Provider.getBalance(signer.address);

      if (gasLimit.gt(blockGasLimit)) {
        console.log('gasLimit', gasLimit);
        throw new Error("Transaction failed #1: insufficient blockGasLimit!");
      }

      if (estimatedCost.gt(balance)) {
        console.log('Balance', balance);
        console.log('EstimatedCost', estimatedCost);
        throw new Error("Transaction failed #2: insufficient funds!");
      }

      const tx = await vesting.connect(signer)[methodName](
        ...methodParams,
        {
          gasLimit: gasLimit.toString(),
        }
      );

      console.log(`Transaction hash: ${tx.hash}`);
      const receipt = await tx.wait();
      console.log(`Transaction mined in block ${receipt.blockNumber}`);
      console.log(`Gas used: ${receipt.gasUsed.toString()}`);
    }
  }
}

function getMethodNameByPool(pool: number): string {
  switch (pool) {
    case Pool.PRIVATE_ROUND_A:
      return 'setPrivateRoundAVestFor';
    case Pool.PRIVATE_ROUND_B:
      return 'setPrivateRoundBVestFor';
    case Pool.COMMUNITY_ROUND:
      return 'setCommunityRoundVestFor';
    case Pool.KOL:
      return 'setKOLVestFor';
    case Pool.MARKETING:
      return 'setMarketingVestFor';
    case Pool.TEAM:
      return 'setTeamVestFor';
    case Pool.ECOSYSTEMRD:
      return 'setEcosystemRDVestFor';
    case Pool.ADVISORS:
      return 'setAdvisorsVestFor';
    default:
      throw new Error(`Unsupported pool: ${pool}`);
  }
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
