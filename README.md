# PROJECT DEPLOYMENT FLOW

1. Clone the project from GitLab
2. Install dependencies
3. Customize configurations
4. Deploy

# 1. Clone the project from GitLab

Enter the following command in the terminal:

```shell
git clone https://gitlab.com/kaif-platform-smart-contracts/kaif-smart-contract.git
```

# 2. Install dependencies

Before launch next command open the terminal into the the main folder of project
Then, enter:

```shell
npm install
```

# 3. Customize configurations

In this project:

1. Rename the .env.example file to a file named .env
2. In the .env file change:

a) Set up API key
- if you deploy in BSC you should set up your BSCScan API key

To get the BSCScan API key, go to
<a href="https://bscscan.com//myapikey">https://bscscan.com/myapikey </a>

b) Your wallet and private key of the account which will send the deployment transaction

# 4. Deploy

<i>Note:</i> before deploying vesting contract in the file <b>deploy-vesting.ts</b> change contract addresses for <b>tokenAddress</b>.

# DEPLOY ON BSC TESTNET

```shell
npx hardhat run scripts/deploy-token.ts --network bscTestnet
npx hardhat run scripts/deploy-vesting.ts --network bscTestnet
```

# DEPLOY ON BSC MAINNET

```shell
npx hardhat run scripts/deploy-token.ts --network bscMainnet
npx hardhat run scripts/deploy-vesting.ts --network bscMainnet
```

# VERIFICATION

Verification is automated

# SCRIPTS

# FOR TOKEN ALLOCATION

```shell
npx hardhat run scripts/token-allocation/set-vest-for.ts
```
# FOR REPORTS

```shell
npx hardhat run scripts/reports/upload-batch-vesting-created-event-report.ts
npx hardhat run scripts/reports/upload-claimed-event-report.ts
```
# FOR VALIDATION ACCOUNTS

```shell
npx hardhat run scripts/data-validation/validate-addresses.ts
```