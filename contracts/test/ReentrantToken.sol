// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

import "./../Vesting.sol";

contract ReentrantToken {
    fallback() external {
        Vesting(msg.sender).withdraw(0);
    }
}
