// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

import "./interface/ITokenVesting.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";

/**
 * @title Vesting to manage tokens between addresses and destinations.
 */
contract Vesting is AccessControl, ITokenVesting, ReentrancyGuard {
    using SafeERC20 for IERC20;

    enum Direction {
        PRIVATE_ROUND_A,
        PRIVATE_ROUND_B,
        COMMUNITY_ROUND,
        KOL,
        MARKETING,
        TEAM,
        ECOSYSTEMRD,
        ADVISORS
    }

    struct VestingSchedule {
        uint128 startAt; // The start date of vesting.
        uint128 cliffInSeconds; //The duration while token locks.
        uint128 vestingInSeconds; // The duration of vesting in seconds.
        uint256 totalAmount; // The amount of vesting token.
        uint16 tge; // The unlock percent of tokens which available in any time.
        uint256 claimed; // The amount of vesting token which was claimed.
    }

    error DataLengthsNotMatch();
    error DataLengthsIsZero();
    error TotalAmountLessThanClaimed();
    error NotEnoughFunds();
    error ClaimAmountIsZero();
    error InsufficientTokens();
    error IncorrectAmount();
    error DurationIsZero();
    error IncorrectTGE();
    error ZeroAddress();
    error NotStarted();

    /**
     * @notice Starter role allows account to start vesting.
     */
    bytes32 public constant STARTER_ROLE = keccak256("STARTER_ROLE");

    /**
     * @notice The number of destinations that participate in the vesting.
     */
    uint8 public constant DIRECTION_COUNT = 8;

    /**
     * @notice Token for distribution.
     */
    IERC20 public immutable token;

    /**
     * @notice Start date of vesting.
     */
    uint128 public startAt;

    /**
     * @notice The total amount of tokens at the addresses that are in the vesting.
     */
    uint256 public vestingSchedulesTotalAmount;

    // Mapping by vesting schedule for a specific address and direction.
    mapping(address => mapping(uint8 => VestingSchedule))
        public vestingSchedules;

    /**
     * @notice Emitted when user claimed tokens.
     * @param account The user address.
     * @param amount The amount of vesting token.
     * @param createdAt The creation date when tokens was claimed.
     * @param direction The number of destination.
     */
    event Claimed(
        address account,
        uint256 amount,
        uint128 createdAt,
        uint8 direction
    );

    /**
     * @notice Emitted when admin created vesting schedules for users.
     * @param accounts The array of users.
     * @param amounts The array of amounts.
     * @param cliff The duration in seconds when token locks.
     * @param vesting The duration of vesting in seconds.
     * @param createdAt The creation date of vesting.
     * @param tge The unlock percent of tokens which available in any time.
     * @param direction The number of destination.
     */
    event BatchVestingCreated(
        address[] accounts,
        uint256[] amounts,
        uint128 cliff,
        uint128 vesting,
        uint128 createdAt,
        uint16 tge,
        uint8 direction
    );

    constructor(address token_, address admin_) {
        token = IERC20(token_);

        // Grants role to 'admin_'.
        _grantRole(DEFAULT_ADMIN_ROLE, admin_);
        // Grants role to 'token_'.
        _grantRole(STARTER_ROLE, token_);
    }

    /**
     * @inheritdoc ITokenVesting
     */
    function setStartAt() external onlyRole(STARTER_ROLE) {
        startAt = uint128(block.timestamp);
    }

    /**
     * @notice Sets community round vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setCommunityRoundVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.COMMUNITY_ROUND)
        );
    }

    /**
     * @notice Sets private A round vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setPrivateRoundAVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.PRIVATE_ROUND_A)
        );
    }

    /**
     * @notice Sets private B round vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setPrivateRoundBVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.PRIVATE_ROUND_B)
        );
    }

    /**
     * @notice Sets KOL vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setKOLVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.KOL)
        );
    }

    /**
     * @notice Sets marketing vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setMarketingVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.MARKETING)
        );
    }

    /**
     * @notice Sets team vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setTeamVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.TEAM)
        );
    }

    /**
     * @notice Sets ecosystem R&D vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setEcosystemRDVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.ECOSYSTEMRD)
        );
    }

    /**
     * @notice Sets advisors vest for user.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     */
    function setAdvisorsVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting
    ) external onlyRole(DEFAULT_ADMIN_ROLE) {
        _batchVestFor(
            _accounts,
            _amounts,
            startAt,
            _tge,
            _cliff,
            _vesting,
            uint8(Direction.ADVISORS)
        );
    }

    /**
     * @notice Claims available vested tokens for all vesting schedules of the user.
     * If there are no tokens available for any direction, the transaction will revert.
     * Emits a `Claimed` event for each direction from which tokens were claimed.
     * Transfers the total claimed amount to the user.
     */
    function claim() external {
        uint256 totalVestedAmount = 0;
        // Iterate over all vesting schedules of the user.
        for (uint8 i = 0; i < DIRECTION_COUNT; i++) {
            VestingSchedule memory schedule = vestingSchedules[msg.sender][i];
            // Calculate the available amount of tokens for the current vesting schedule.
            uint256 vestedAmount = _vestedAmount(schedule);

            if (vestedAmount > 0) {
                // Increases released amount in vesting.
                vestingSchedules[msg.sender][i].claimed =
                    vestedAmount +
                    schedule.claimed;

                emit Claimed(msg.sender, vestedAmount, uint128(block.timestamp), i);
            }

            totalVestedAmount += vestedAmount;
        }

        if (totalVestedAmount == 0) {
            revert ClaimAmountIsZero();
        }

        // Current amount of tokens in vesting.
        vestingSchedulesTotalAmount -= totalVestedAmount;

        token.safeTransfer(msg.sender, totalVestedAmount);
    }

    /**
     * @notice Withdraws available amount of tokens in the contract.
     * @param _amount The amount of tokens.
     */
    function withdraw(
        uint256 _amount
    ) external nonReentrant onlyRole(DEFAULT_ADMIN_ROLE) {
        if (getWithdrawableAmount() < _amount) {
            revert NotEnoughFunds();
        }

        token.safeTransfer(msg.sender, _amount);
    }

    /**
     * @notice Returns the total vesting information for a given account.
     * @param _account The user address.
     * @return totalAmount The total amount of tokens in vesting schedules.
     * @return unlockedAmount The amount of tokens currently unlocked.
     * @return claimedAmount The amount of tokens already claimed.
     * @return lockedAmount The amount of tokens still locked.
     */
    function getTotalVestingInfo(
        address _account
    )
        external
        view
        returns (
            uint256 totalAmount,
            uint256 unlockedAmount,
            uint256 claimedAmount,
            uint256 lockedAmount
        )
    {
        for (uint8 i = 0; i < DIRECTION_COUNT; i++) {
            VestingSchedule memory schedule = vestingSchedules[_account][i];

            unlockedAmount += _vestedAmount(schedule);
            totalAmount += schedule.totalAmount;
            claimedAmount += schedule.claimed;
        }

        lockedAmount = totalAmount - claimedAmount - unlockedAmount;
    }

    /**
     * @notice Returns withdrawable amount of tokens which is available.
     */
    function getWithdrawableAmount() public view returns (uint256) {
        return token.balanceOf(address(this)) - vestingSchedulesTotalAmount;
    }

    /**
     * @notice Creates vesting schedules for user.
     * @param _account The user address.
     * @param _amount The amount of vesting token.
     * @param _startAt The start date of vesting.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     * @param _direction The direction of vesting.
     */
    function _vestFor(
        address _account,
        uint256 _amount,
        uint128 _startAt,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting,
        uint8 _direction
    ) private {
        if (getWithdrawableAmount() < _amount) {
            revert InsufficientTokens();
        }
        if (_amount == 0) {
            revert IncorrectAmount();
        }
        if (_vesting == 0) {
            revert DurationIsZero();
        }
        if (_tge > 10000) {
            revert IncorrectTGE();
        }
        if (_account == address(0)) {
            revert ZeroAddress();
        }
        if (_startAt == 0) {
            revert NotStarted();
        }

        uint256 totalAmount = vestingSchedules[_account][_direction]
            .totalAmount;
        uint256 claimed = vestingSchedules[_account][_direction].claimed;

        if (totalAmount == 0) {
            vestingSchedules[_account][_direction].startAt = _startAt;

            // Current amount of tokens in vesting.
            vestingSchedulesTotalAmount += _amount;
        } else {
            if (_amount < claimed) {
                revert TotalAmountLessThanClaimed();
            }

            // Current amount of tokens in vesting if totalAmount was modified.
            vestingSchedulesTotalAmount =
                vestingSchedulesTotalAmount -
                (totalAmount - claimed) +
                _amount;
        }
        vestingSchedules[_account][_direction].cliffInSeconds = _cliff;
        vestingSchedules[_account][_direction].vestingInSeconds = _vesting;
        vestingSchedules[_account][_direction].totalAmount = _amount;
        vestingSchedules[_account][_direction].tge = _tge;
        vestingSchedules[_account][_direction].claimed = claimed;
    }

    /**
     * @notice Creates vesting schedules for users.
     * @param _accounts The array of users.
     * @param _amounts The array of amounts.
     * @param _startAt The start date of vesting.
     * @param _tge The unlock percent of tokens which available in any time.
     * @param _cliff The duration in seconds when token locks.
     * @param _vesting The duration of vesting in seconds.
     * @param _direction The direction of vesting.
     */
    function _batchVestFor(
        address[] calldata _accounts,
        uint256[] calldata _amounts,
        uint128 _startAt,
        uint16 _tge,
        uint128 _cliff,
        uint128 _vesting,
        uint8 _direction
    ) private {
        uint16 accountsCount = uint16(_accounts.length);

        if (accountsCount == 0) {
            revert DataLengthsIsZero();
        }

        if (accountsCount != _amounts.length) {
            revert DataLengthsNotMatch();
        }

        for (uint16 i = 0; i < accountsCount; i++) {
            _vestFor(
                _accounts[i],
                _amounts[i],
                _startAt,
                _tge,
                _cliff,
                _vesting,
                _direction
            );
        }

        emit BatchVestingCreated(
            _accounts,
            _amounts,
            _cliff,
            _vesting,
            uint128(block.timestamp),
            _tge,
            _direction
        );
    }

    /**
     * @notice Returns available amount of tokens.
     * @param _vestingSchedule The vesting schedule structure.
     */
    function _vestedAmount(
        VestingSchedule memory _vestingSchedule
    ) private view returns (uint256) {
        uint256 totalAmount = _vestingSchedule.totalAmount;

        if (totalAmount == 0) {
            return 0;
        }

        uint256 claimed = _vestingSchedule.claimed;
        // The unlock amount of tokens which available in any time.
        uint256 tgeAmount = (totalAmount * _vestingSchedule.tge) / 1e4;
        // Duration in seconds from starting vesting.
        uint128 passedTimeInSeconds = uint128(block.timestamp) -
            _vestingSchedule.startAt;

        uint128 cliffInSeconds = _vestingSchedule.cliffInSeconds;
        uint128 vestingInSeconds = _vestingSchedule.vestingInSeconds;
        uint128 maxPossibleTime = vestingInSeconds + cliffInSeconds;

        if (passedTimeInSeconds > maxPossibleTime) {
            passedTimeInSeconds = maxPossibleTime;
        }

        if (passedTimeInSeconds <= cliffInSeconds) {
            passedTimeInSeconds = 0;
        } else {
            passedTimeInSeconds = passedTimeInSeconds - cliffInSeconds;
        }

        uint256 payout = tgeAmount +
            ((totalAmount - tgeAmount) * passedTimeInSeconds) /
            vestingInSeconds;

        if (claimed >= payout) {
            return 0;
        } else {
            return payout - claimed;
        }
    }
}
